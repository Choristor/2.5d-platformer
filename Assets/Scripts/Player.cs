﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Player : MonoBehaviour
{
    public GameObject spawnPoint;
    public Stats stats;
    public GameObject dieCarrot;
    private Animation dieCarrotAnimation;
    public GameObject canvas;
    private PlayerUI playerUI;
    // Start is called before the first frame update
    void Start()
    {
        Debug.Log("Player");
        playerUI = canvas.GetComponent<PlayerUI>();
        dieCarrotAnimation = dieCarrot.GetComponent<Animation>();
        StartCoroutine(LateStart(0.1f));
        
    }
    IEnumerator LateStart(float waitTime)
    {
        yield return new WaitForSeconds(waitTime);

        playerUI.Refresh(stats.lives, stats.points);
    }
    // Update is called once per frame
    void Update()
    {
        if (Input.GetButton("Reset"))
        {

            SceneManager.LoadScene("SampleScene");
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Collectable")
        {
            stats.addPoints(other.GetComponent<Collectable>().point);

            playerUI.Refresh(stats.lives, stats.points);
            Destroy(other.gameObject);

        }
        if (other.tag == "Target")
        {
            stats.addPoints(other.GetComponent<Collectable>().point);
            Destroy(other.gameObject);
        }
    
        if (other.tag.Equals("DieZone"))
        {

            stats.loseLife();

            playerUI.Refresh(stats.lives, stats.points);
            Respawn();
        }
        if (other.tag.Equals("Finish"))
        {
            GameManager.NextLevel();
        }
    }


    public void Respawn()
    {
        dieCarrot.SetActive(true);
        dieCarrot.GetComponent<Animation>().Play();

        StartCoroutine(WaitForAnimation(dieCarrotAnimation));

        transform.position = spawnPoint.transform.position;
        GetComponent<Rigidbody>().velocity = Vector3.zero;

        GetComponent<Rigidbody>().angularVelocity = Vector3.zero;
    }
    private IEnumerator WaitForAnimation(Animation animation)
    {
        do
        {
            yield return null;
        } while (animation.isPlaying);

        dieCarrot.SetActive(false);

    }
}
