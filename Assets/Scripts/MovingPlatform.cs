﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingPlatform : MonoBehaviour
{
    public List<Transform> positions;
    public GameObject positionTransforms;
    public GameObject platform;
    public int nextPoint;
    public float speed = 1.0f;
    // Start is called before the first frame update
    void Start()
    {
        positions = new List<Transform>(positionTransforms.GetComponentsInChildren<Transform>());
        positions.RemoveAt(0);
    }

    // Update is called once per frame
    void Update()
    {
        platform.transform.position=Vector3.MoveTowards(platform.transform.position, positions[nextPoint].position, speed*0.2f*Time.deltaTime);
        
        if (Vector3.Distance(platform.transform.position, positions[nextPoint].position) < 0.001f)
        {
            ++nextPoint;
            if (nextPoint >= positions.Count)
            {
                nextPoint = 0;
            }
            
        }
    }
}
