﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[CreateAssetMenu(fileName ="Stats", menuName ="ScriptableObjects/Stats",order =1)]
public class Stats : ScriptableObject
{
    public int lives = 3;
    public int points = 0;
    private void OnEnable()
    {
    }
    public int Points {
        get { return points; }
        set {
            points = value;
        }
    }
    const int POINTS_PER_LIFE=100000;
    public void addPoints(int number)
    {
        Points += number;
        if (Points >= POINTS_PER_LIFE)
        {
            lives += 1;
            Points -= POINTS_PER_LIFE;
        }
    }

    public void loseLife()
    {
        lives -= 1;
    }
}
