﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerUI : MonoBehaviour
{
    public GameObject canvas;
    public GameObject lifeCounter;
    public GameObject pointCounter;
    public Text lifeCounterText;
    public Text pointCounterText;

    private void OnEnable()
    {

        lifeCounterText = lifeCounter.GetComponent<Text>();
        pointCounterText = pointCounter.GetComponent<Text>();
    }
    // Start is called before the first frame update
    void Start()
    {
        Debug.Log("Player UI");

        lifeCounterText.text = "0";
        pointCounterText.text = "0";
    }

    public void Refresh(int lifesCount,int pointsCount)
    {


        lifeCounterText.text = lifesCount+"";
        pointCounterText.text = pointsCount + "";


    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
