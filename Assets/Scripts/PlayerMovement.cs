﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    bool grounded = true;
    [Range(0,10)]
    public float hrs=1.0f;
    [Range(0,10)]
    public float vrs = 1.0f;
    [Range(0, 10)]
    public float climbingSpeed = 1.0f;
    public Rigidbody rb;
    
    public bool isClimbing = false;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        float hrz = Input.GetAxis("Horizontal");
        float vrz = Input.GetAxis("Jump");

       rb.MovePosition(new Vector3(transform.position.x+hrz * Time.deltaTime*hrs,transform.position.y, 0));
      
        if(Input.GetButton("Up") && isClimbing)
        {
            transform.position += Vector3.up*Time.deltaTime*climbingSpeed;
        }


    }
    private void FixedUpdate()
    {
        if (Input.GetButtonUp("Jump") && grounded)
        {
            GetComponent<Rigidbody>().AddForce(Vector3.up * vrs * 100);
            
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag.Equals("Ground"))
        {
            grounded = true;
        }


    }
    private void OnTriggerStay(Collider other)
    {
        if (other.tag.Equals("Ground"))
        {
            grounded = true;
        }
        if(other.tag.Equals("Ladder") && Input.GetButton("Climb")&& !isClimbing)
        {
            isClimbing = true;
            rb.constraints = RigidbodyConstraints.FreezeRotation | RigidbodyConstraints.FreezePositionY | RigidbodyConstraints.FreezePositionZ;
        }
        else if (other.tag.Equals("Ladder")&&isClimbing && Input.GetButton("Climb"))
        {
            isClimbing = false;
            rb.constraints = RigidbodyConstraints.FreezeRotation | RigidbodyConstraints.FreezePositionZ;

        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.tag.Equals("Ground"))
        {
            grounded = false;
        }
        if (isClimbing && other.tag.Equals("Ladder"))
        {
            isClimbing = false;
            rb.constraints = RigidbodyConstraints.FreezeRotation  | RigidbodyConstraints.FreezePositionZ;

        }
    }

}
