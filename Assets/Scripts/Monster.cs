﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Monster : MonoBehaviour
{
    [Range(0.5f,20.0f)]
    public float speed = 1.0f;
    public bool wayLeft;
    public Rigidbody rb;
    public bool isClever = true;
    GameManager gm;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {/*
        if (wayLeft)
        {
            rb.AddForce(Vector3.left * speed);
        }else if (!wayLeft)
        {
            rb.AddForce(Vector3.right * speed);
        }*/
    }
    private void OnTriggerEnter(Collider other)
    {
        /*
        if (other.tag.Equals("Barrier")&&isClever)
        {
            wayLeft = !wayLeft;
            rb.angularVelocity = Vector3.zero;
            rb.velocity = Vector3.zero;
        }*/
        if (other.tag.Equals("Player"))
        {
            other.GetComponent<Player>().Respawn();
        }

    }
}
